using Xunit;
using covariance_and_interface;

namespace tests
{
    public class Tests
    {
        [Fact]
        public void MyMcDonaldsOrder_ShouldCost_ElevenNinetyFive()
        {
            //Arrange
            var fakedatabase = new FakeDatabase();
           
            //Act
            Program.MyMcDonaldsOrder(fakedatabase);

            //Assert
            Assert.Equal(fakedatabase.LastReceivedTransaction.Amount, (decimal)11.95);
        }
    }
}