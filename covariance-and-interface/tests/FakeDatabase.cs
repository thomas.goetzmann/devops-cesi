using covariance_and_interface;

namespace tests
{
    public class FakeDatabase : IDatabase
    {
        public Transaction LastReceivedTransaction {get; private set;}

        public string Save(Transaction transaction) 
        {
            LastReceivedTransaction = transaction;
            return "ok";
        } 
    }
}