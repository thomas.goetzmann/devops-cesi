using System;

namespace covariance_and_interface
{
    public interface IDatabase{
        string Save(Transaction transaction);
    }

    public class MySqlDatabase : IDatabase
    {
        public string Save(Transaction transaction)
        {
            //SORRY: My manager told me to ship it like this !
            throw new NotImplementedException();
        }
    }

    public class MongoDbDatabase : IDatabase
    {
        public string Save(Transaction transaction)
        {
            return $"SAVED: {transaction.Amount} from {transaction.Buyer} to {transaction.Vendor}";
        }

    }

    public class Transaction
    {
        public Guid Id => Guid.NewGuid();
        public string Buyer { get; set; }
        public decimal Amount { get; set; }
        public string Vendor { get; set; }
    }
}