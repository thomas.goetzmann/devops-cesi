﻿using System;
using System.Collections.Generic;

namespace covariance_and_interface
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //CovarianceExplained();
            //MyMcDonaldsOrder(new MySqlDatabase());
        }

        public static void CovarianceExplained()
        {
            List<Ferrari> ferraris = new List<Ferrari>
            { 
                new Ferrari{},
                new Ferrari{}
            };

            foreach(var ferrari in ferraris)
            {
                ferrari.StartEngine();
            }
            Console.WriteLine("###############");

            //TODO ... list, ienum...
        }

        public static void MyMcDonaldsOrder(MySqlDatabase database)
        {
            var maxiMcfirst_plus_Deluxe = new Transaction{
                Buyer = "Thomas",
                Vendor = "McDonalds",
                Amount =  (decimal)11.95
            };
            
            string result = database.Save(maxiMcfirst_plus_Deluxe);

            Console.WriteLine(result);
        }
    }
}
