using System;

namespace covariance_and_interface
{

    public interface Car
    {
        void StartEngine();
    }

    public class Ferrari : Car
    {
        public void StartEngine()
        {
            Console.WriteLine("Vroummmm");
        }
    }
}