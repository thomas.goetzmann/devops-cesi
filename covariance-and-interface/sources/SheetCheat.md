# SheetCheat (just in case I forget something)

## Covariance

```C#
    List<Ferrari> ferraris = new List<Ferrari>();
    //[...]
    List<Car> cars = ferraris; //not possible because invariant
    IEnumerable<Car> cars = ferraris; // possible because variant
    //[...]
``` 

## Interface

```C#
    //Transform

    //1. Method signature
    static void MyMcDonaldsOrder(MySqlDatabase database)
    static void MyMcDonaldsOrder(IDatabase database)
    
    //2. In main method
    MyMcDonaldsOrder(new MySqlDatabase());
    MyMcDonaldsOrder(new MongoDbDatabase());
```